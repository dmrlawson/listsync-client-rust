all: test

fmt:
	cargo fmt

build: fmt
	cargo build

test: build
	cargo test

DEPL := listsync_webspace:listsync-client-rust

deploy:
	cargo build --release
	scp target/release/listsync-client-rust ${DEPL}/listsync-client-rust.new
	scp contact-artificialworlds.html ${DEPL}/contact.html
