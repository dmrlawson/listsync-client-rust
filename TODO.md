# TODO

See also [Server
TODO](https://gitlab.com/listsync/listsync-server-rust/-/blob/master/TODO.md).

## Version 1.0.0

- [ ] Remove background image and make backgrounds non-transparent
- [ ] Link to favicon
- [ ] Rename "Home" to "Lists"?
- [ ] Make log in and register 1 page, with equal weight
- [ ] Move user actions (e.g. "delete myself") to separate page
- [ ] Consistent tags, CSS classes and (contrasting) colours for:
    - title
    - nav
    - main content
    - extra content
    - damaging actions
    - etc.

## Finishing

- [ ] Refactor messages at top of screen
- [ ] Refactor CSS, remove repeats
- [ ] Log using "info!()" etc.
- [ ] Set cookie "path" to prefix
- [ ] -Admin- List users
- [ ] -Admin- Create a user
- [ ] -Admin- Delete users
- [ ] -Admin- Setup mode
- [ ] Do all TODOs
- [ ] Login page sends you to wherever you tried to get to
- [ ] No need to log in after registration
- [ ] Encrypt cookie with a private key stored on server filesystem
- [ ] Delay on failed login, based on originating IP or username
- [ ] Upstream my enhancements and fixes to actix-session
- [ ] Separate the user's user page from their lists page?
- [ ] Monitoring: notice new users
- [ ] Monitoring: notice if not running
- [ ] Monitoring: track memory usage
- [ ] Monitoring: track number of users, lists, requests
- [ ] Monitoring: notice if slow
- [ ] Archive logs and copy off server

## Later

- Registration via email - consider deleting GET /v1/newuser/{username}
- Unregistered users exire
- Request new registration code
- Share lists (view-only and editable)

# Done

## Public beta v0.9.0

- [x] Friendly list names
- [x] Store token in cookie instead of password
- [x] "Remember me" on log in
- [x] List IDs instead of listnames
- [x] Rename list
- [x] Custom path prefix
- [x] Publish to web space
- [x] Fix small checkboxes in Chrome
- [x] Display "Save to add more..." under items
- [x] Home button in top left at all times
- [x] Log out
- [x] Cookie warning on login
- [x] Delete auth token on log out
- [x] User registration
- [x] Home emoji
- [x] Privacy info (including cookies)
- [x] Change password
- [x] Delete yourself and all lists

## Early prototype, invite-only

- [x] Experiment with browser built-in HTTP Basic Auth
- [x] Log in, storing username and password in cookies
- [x] Display list of lists
- [x] Display items in list
- [x] Update items in list
- [x] Redirect to /user when going to /
- [x] Create a new list
- [x] Delete a list
- [x] Header linking to user page
- [x] CSS to make all pages pretty
- [x] Server admin contact info
- [x] Error pages when things go wrong

