## Notes to self about deployment.

Build and upload:

```bash
make deploy
```

Also make sure there is a `contact.html` file inside the `listsync-client-rust`
directory.

There is a screen running in my webspace.  To attach:

```bash
screen -r
```

Make sure you are in the right screen for this program (screen 1), then press
Ctrl-c and:

```
mv listsync-client-rust.new listsync-client-rust
./listsync-client-rust --path-prefix=ui
```

I have set up my web provider to proxy
https://listsync.artificialworlds.net/ui to the process running in the screen,
on port 8089.
