use std::fs::File;
use std::io::Read;

#[derive(Clone)]
pub struct ContactPage {
    pub contents: String,
}

const DEFAULT: &str = r#"The contact details for the server administrator
have not been provided.  Please get in touch with the person who set up this
server, or see
<a href="https://gitlab.com/listsync/listsync-client-rust">gitlab.com/listsync/listsync-client-rust</a>
for more information about the software that is running on this server."#;

impl ContactPage {
    pub fn new(contents: String) -> ContactPage {
        ContactPage { contents }
    }

    pub fn load(filename: &str) -> ContactPage {
        let file = File::open(filename);

        if let Ok(mut file) = file {
            let mut contents = String::new();
            let res = file.read_to_string(&mut contents);
            if let Ok(_) = res {
                println!("Loaded contact info from {}", filename);
                return ContactPage::new(contents);
            }
        }
        println!(
            "No contact info was provided - create a file called {}",
            filename
        );
        ContactPage::new(String::from(DEFAULT))
    }
}
