use actix_web::{web, FromRequest, HttpRequest};
use serde_json::json;

use crate::actix_session_remember::Session;
use crate::data_holder::DataHolder;
use crate::login_credentials::LoginCredentials;
use crate::lsc_error::LscError;
use crate::urls::Urls;

pub struct Credentials {
    pub username: String,
    pub token: String,
}

impl From<&LoginCredentials> for Credentials {
    fn from(login_credentials: &LoginCredentials) -> Credentials {
        Credentials {
            username: String::from(&login_credentials.username),
            token: String::from(&login_credentials.password),
        }
    }
}

impl Credentials {
    pub async fn extract(
        req: &HttpRequest,
        urls: &Urls,
    ) -> Result<Credentials, LscError> {
        Session::extract(req)
            .await
            .map_err(|_| LscError::no_credentials_provided(req, urls))
            .and_then(|session| {
                if let (Some(username), Some(token)) = (
                    session.get("username").unwrap_or(None),
                    session.get("token").unwrap_or(None),
                ) {
                    Ok(Credentials { username, token })
                } else {
                    Err(LscError::no_credentials_provided(req, urls))
                }
            })
    }

    pub async fn save(
        req: &HttpRequest,
        urls: &Urls,
        login_credentials: &LoginCredentials,
    ) -> Result<(), LscError> {
        if let Ok(session) = Session::extract(req).await {
            let data_holder =
                web::Data::<DataHolder>::extract(&req).await.unwrap();
            data_holder
                .api
                .post(
                    &login_credentials.into(),
                    "/v1/login",
                    json!( { "remember_me": login_credentials.remember_me, }),
                )
                .await
                .map_err(|e| LscError::from_api_error(req, urls, e))
                .and_then(|res| {
                    if let (Some(username), Some(token)) =
                        (res.get("username"), res.get("token"))
                    {
                        session.set("username", &username).unwrap();
                        session.set("token", &token).unwrap();
                        if login_credentials.remember_me {
                            session.remember_me(true);
                        }
                        Ok(())
                    } else {
                        Err(LscError::internal_error(
                            req,
                            urls,
                            &format!(
                                "Failed to extract token from JSON: {}",
                                res
                            ),
                        ))
                    }
                })
        } else {
            Err(LscError::internal_error(
                req,
                urls,
                "Failed to find the user session.",
            ))
        }
    }
}
