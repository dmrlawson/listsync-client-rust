use actix_web::{Error, HttpRequest, HttpResponse, Responder};
use futures::future::{ready, Ready};
use typed_html::dom::DOMTree;
use typed_html::elements;

pub struct DOMTreeResponder {
    domtree: DOMTree<String>,
}

impl From<Box<elements::html<String>>> for DOMTreeResponder {
    fn from(domtree: Box<elements::html<String>>) -> Self {
        DOMTreeResponder { domtree }
    }
}

impl From<DOMTree<String>> for DOMTreeResponder {
    fn from(domtree: DOMTree<String>) -> Self {
        DOMTreeResponder { domtree }
    }
}

impl Responder for DOMTreeResponder {
    type Error = Error;
    type Future = Ready<Result<HttpResponse, Error>>;
    fn respond_to(self, _req: &HttpRequest) -> Self::Future {
        ready(Ok(HttpResponse::Ok()
            .content_type("text/html; charset=UTF-8")
            .body(format!(
                "<!DOCTYPE html>\n{}",
                self.domtree.to_string()
            ))))
    }
}
