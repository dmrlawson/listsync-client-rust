use typed_html::dom::{DOMTree, UnsafeTextNode};
use typed_html::unsafe_text;

pub fn insert_domtree(content: DOMTree<String>) -> Box<UnsafeTextNode<String>> {
    unsafe_text!(content.to_string())
}
