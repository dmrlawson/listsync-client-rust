use actix_web::body::Body;
use actix_web::http::StatusCode;
use actix_web::{http, HttpRequest, HttpResponse, ResponseError};
use std::fmt::{Display, Formatter};
use typed_html::dom::DOMTree;
use typed_html::html;
use url::Url;

use crate::api::ApiError;
use crate::urls::Urls;

#[derive(Debug)]
pub enum LscError {
    AuthenticationFailed(Url),
    Forbidden(Url),
    InternalError {
        home_url: Url,
        contact_url: Url,
        style_css_url: Url,
    },
    NoCredentialsProvided(Url),
}

impl LscError {
    pub fn no_credentials_provided(req: &HttpRequest, urls: &Urls) -> LscError {
        LscError::NoCredentialsProvided(
            urls.url_for_static(req, "login").unwrap(),
        )
    }
    pub fn authentication_failed(req: &HttpRequest, urls: &Urls) -> LscError {
        let mut url = urls.url_for_static(req, "login").unwrap();
        url.set_query(Some("failed=true"));
        LscError::AuthenticationFailed(url)
    }
    pub fn forbidden(req: &HttpRequest, urls: &Urls) -> LscError {
        let mut url = urls.url_for_static(req, "root").unwrap();
        url.set_query(Some("failed=true"));
        LscError::Forbidden(url)
    }
    pub fn internal_error(
        req: &HttpRequest,
        urls: &Urls,
        private_log_message: &str,
    ) -> LscError {
        println!("Internal error at {}: {}", req.path(), private_log_message);
        LscError::InternalError {
            home_url: urls.url_for_static(req, "root").unwrap(),
            contact_url: urls.url_for_static(req, "contact").unwrap(),
            style_css_url: urls.url_for_static(req, "style_css").unwrap(),
        }
    }
    pub fn from_api_error(
        req: &HttpRequest,
        urls: &Urls,
        api_error: ApiError,
    ) -> LscError {
        match api_error {
            ApiError::AuthenticationFailed => {
                LscError::authentication_failed(req, urls)
            }
            ApiError::Forbidden => LscError::forbidden(req, urls),
            ApiError::InternalError => LscError::internal_error(
                req,
                urls,
                "API returned Internal Error.",
            ),
            ApiError::UserAlreadyExists => {
                LscError::internal_error(req, urls, "User already exists.")
            }
        }
    }
}

impl Display for LscError {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        write!(
            f,
            "{}",
            match self {
                LscError::AuthenticationFailed(_) =>
                    "Supplied login details are not correct.",
                LscError::Forbidden(_) =>
                    "You do not have permission to do this.",
                LscError::InternalError {
                    home_url: _,
                    contact_url: _,
                    style_css_url: _,
                } => "Internal error",
                LscError::NoCredentialsProvided(_) => "Login is required.",
            }
        )
    }
}

impl ResponseError for LscError {
    fn status_code(&self) -> StatusCode {
        match self {
            LscError::AuthenticationFailed(_) => StatusCode::UNAUTHORIZED,
            LscError::Forbidden(_) => StatusCode::FORBIDDEN,
            LscError::InternalError {
                home_url: _,
                contact_url: _,
                style_css_url: _,
            } => StatusCode::INTERNAL_SERVER_ERROR,
            LscError::NoCredentialsProvided(_) => StatusCode::UNAUTHORIZED,
        }
    }
    fn error_response(&self) -> HttpResponse<Body> {
        match self {
            LscError::AuthenticationFailed(url) => redirect_response(url),
            LscError::Forbidden(url) => redirect_response(url),
            LscError::InternalError {
                home_url,
                contact_url,
                style_css_url,
            } => internal_error_response(home_url, contact_url, style_css_url),
            LscError::NoCredentialsProvided(url) => redirect_response(url),
        }
    }
}

fn internal_error_response(
    home_url: &Url,
    contact_url: &Url,
    style_css_url: &Url,
) -> HttpResponse {
    let page: DOMTree<String> = html!(
        <html>
            <head>
                <title>"Internal error - listsync HTML"</title>
                <meta charset="utf-8"/>
                <meta
                    name="viewport"
                    content="width=device-width,initial-scale=1"
                />
                <link href={style_css_url.to_string()} rel="stylesheet"/>
            </head>
            <body>
                <div id="whole-page-div">
                    <div id="internal-error-div">
                        <h1>"Internal error"</h1>
                        <div>
                            "Sorry, something went wrong.  Please try again,
                            and if that doesn't work, go back to "
                            <a href={home_url.to_string()}>"\u{1F3E0} Home"</a>
                            " or "
                            <a href={contact_url.to_string()}>
                                "contact the server administrators"
                            </a>
                            "."
                        </div>
                    </div>
                </div>
            </body>
        </html>
    );
    HttpResponse::InternalServerError().body(page.to_string())
}

fn redirect_response(url: &Url) -> HttpResponse {
    HttpResponse::Found()
        .header(http::header::LOCATION, url.to_string())
        .finish()
        .into_body()
}
