use actix_web::{web, HttpRequest};
use typed_html::dom::DOMTree;
use typed_html::{html, unsafe_text};

use crate::data_holder::DataHolder;
use crate::domtree_responder::DOMTreeResponder;
use crate::lsc_error::LscError;
use crate::page::page;
use crate::urls::Urls;

pub async fn get(
    req: HttpRequest,
    urls: Urls,
    data_holder: web::Data<DataHolder>,
) -> Result<DOMTreeResponder, LscError> {
    // Note: no credentials needed for this page.
    Ok(page("Contact", None, contact_info(&data_holder), &req, &urls).into())
}

fn contact_info(data_holder: &DataHolder) -> DOMTree<String> {
    html!(
        <div class="info-container-div">
            <h1>"Contact"</h1>
            <div class="info-div">
                {unsafe_text!(&data_holder.contact_page.contents)}
            </div>
        </div>
    )
}
