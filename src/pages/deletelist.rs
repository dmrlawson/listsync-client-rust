use actix_web::http;
use actix_web::{web, HttpRequest, HttpResponse};
use serde::Deserialize;

use crate::credentials::Credentials;
use crate::data_holder::DataHolder;
use crate::lsc_error::LscError;
use crate::urls::Urls;

#[derive(Deserialize)]
pub struct Path {
    username: String,
    listid: String,
}

#[derive(Deserialize)]
pub struct DeleteList {
    pub sure: Option<String>,
}

pub async fn post(
    req: HttpRequest,
    urls: Urls,
    path: web::Path<Path>,
    delete_list: web::Form<DeleteList>,
    data_holder: web::Data<DataHolder>,
) -> Result<HttpResponse, LscError> {
    match Credentials::extract(&req, &urls).await {
        Ok(credentials) => match &delete_list.sure {
            Some(on) => {
                if on == "on" {
                    delete_list_and_redirect(
                        &req,
                        &urls,
                        &path,
                        &data_holder,
                        &credentials,
                    )
                    .await
                } else {
                    Ok(redirect_to_list(&req, &urls, &path).await)
                }
            }
            _ => Ok(redirect_to_list(&req, &urls, &path).await),
        },
        Err(e) => Err(e),
    }
}

async fn delete_list_and_redirect(
    req: &HttpRequest,
    urls: &Urls,
    path: &Path,
    data_holder: &DataHolder,
    credentials: &Credentials,
) -> Result<HttpResponse, LscError> {
    let url = format!("/v1/list/{}/{}", path.username, path.listid);
    data_holder
        .api
        .delete(&credentials, &url, None)
        .await
        .map_err(|e| LscError::from_api_error(req, urls, e))
        .map(|_| {
            let mut re_url = urls
                .url_for(req, "user", vec![&credentials.username])
                .unwrap();
            re_url.set_query(Some("message=list_deleted"));
            HttpResponse::Found()
                .header(http::header::LOCATION, re_url.to_string())
                .finish()
                .into_body()
        })
}

async fn redirect_to_list(
    req: &HttpRequest,
    urls: &Urls,
    path: &Path,
) -> HttpResponse {
    let mut redir_url = urls
        .url_for(req, "list", vec![&path.username, &path.listid])
        .unwrap();
    redir_url.set_query(Some("error=sure_required"));

    HttpResponse::Found()
        .header(http::header::LOCATION, redir_url.to_string())
        .finish()
        .into_body()
}
