use actix_web::http;
use actix_web::{web, HttpRequest, HttpResponse, Responder};
use serde::Deserialize;
use typed_html::dom::UnsafeTextNode;
use typed_html::elements::div;
use typed_html::{html, text, unsafe_text};

use crate::credentials::Credentials;
use crate::data_holder::DataHolder;
use crate::domtree_responder::DOMTreeResponder;
use crate::lsc_error::LscError;
use crate::page::page;
use crate::urls::Urls;

#[derive(Deserialize)]
pub struct Path {
    username: String,
}

#[derive(Deserialize)]
pub struct DeleteUser {
    pub sure: Option<String>,
    pub really_sure: Option<String>,
}

pub async fn post(
    req: HttpRequest,
    urls: Urls,
    path: web::Path<Path>,
    delete_user: web::Form<DeleteUser>,
    data_holder: web::Data<DataHolder>,
) -> Result<HttpResponse, LscError> {
    match Credentials::extract(&req, &urls).await {
        Ok(credentials) => {
            if let Some(on) = &delete_user.sure {
                if on == "on" {
                    if let Some(really_on) = &delete_user.really_sure {
                        if really_on == "on" {
                            return delete_user_and_redirect(
                                &req,
                                &urls,
                                &path,
                                &data_holder,
                                &credentials,
                            )
                            .await;
                        }
                    }
                    return display_deleteuser(
                        &req,
                        &urls,
                        &path,
                        &credentials,
                        Some(html!(
                            <div class="error">
                                r#"Confirm by choosing "I am REALLY sure"."#
                            </div>
                        )),
                    )
                    .await;
                }
            };
            Ok(redirect_to_user(&req, &urls, &path).await)
        }
        Err(e) => Err(e),
    }
}

async fn delete_user_and_redirect(
    req: &HttpRequest,
    urls: &Urls,
    path: &Path,
    data_holder: &DataHolder,
    credentials: &Credentials,
) -> Result<HttpResponse, LscError> {
    let url = format!("/v1/user/{}", path.username);
    data_holder
        .api
        .delete(&credentials, &url, None)
        .await
        .map_err(|e| LscError::from_api_error(req, urls, e))
        .map(|_| {
            let mut re_url = urls.url_for_static(req, "login").unwrap();
            re_url.set_query(Some("message=user_deleted"));
            HttpResponse::Found()
                .header(http::header::LOCATION, re_url.to_string())
                .finish()
                .into_body()
        })
}

async fn redirect_to_user(
    req: &HttpRequest,
    urls: &Urls,
    path: &Path,
) -> HttpResponse {
    let mut redir_url =
        urls.url_for(req, "user", vec![&path.username]).unwrap();
    redir_url.set_query(Some("error=sure_required"));

    HttpResponse::Found()
        .header(http::header::LOCATION, redir_url.to_string())
        .finish()
        .into_body()
}

async fn display_deleteuser(
    req: &HttpRequest,
    urls: &Urls,
    path: &Path,
    credentials: &Credentials,
    message: Option<Box<div<String>>>,
) -> Result<HttpResponse, LscError> {
    disp_del_user(req, urls, path, credentials, message)
        .respond_to(req)
        .await
        .map_err(|_| {
            LscError::internal_error(
                req,
                urls,
                "Failed to turn deleteuser HTML into a response",
            )
        })
}

fn display_message(
    message: Option<Box<div<String>>>,
) -> Box<UnsafeTextNode<String>> {
    unsafe_text!(message.map(|m| m.to_string()).unwrap_or(String::from("")))
}

fn disp_del_user(
    req: &HttpRequest,
    urls: &Urls,
    path: &Path,
    credentials: &Credentials,
    message: Option<Box<div<String>>>,
) -> DOMTreeResponder {
    page(
        "Delete user?",
        Some(&credentials.username),
        html!(
            <div>
                {display_message(message)}
                <div class="secondary-section">
                    <p>
                        "Delete user "
                        {text!(&path.username)}
                        "?"
                    </p>
                    <form
                        id="delete-user-form"
                        class="secondary-section"
                        action={
                            urls.url_for(
                                req,
                                "deleteuser",
                                vec![&path.username]
                            ).unwrap().to_string()
                        }
                        method="post"
                    >
                        <button name="delete-user">
                            "Delete myself and all my lists"
                        </button>
                        <input type="hidden" name="sure" value="on"/>
                        <input
                            type="checkbox"
                            name="really_sure"
                            id="really_sure"
                        />
                        <label for="really_sure">"I am REALLY sure"</label>
                    </form>
                </div>
            </div>
        ),
        req,
        urls,
    )
    .into()
}
