use actix_web::{http, HttpResponse};

pub async fn get() -> HttpResponse {
    HttpResponse::Ok()
        .content_type("image/jpeg")
        .header(http::header::CACHE_CONTROL, "max-age=2419200")
        .body(include_bytes!("kolar-io-ICOdHM1Cuvg-unsplash.jpg")
            as &'static [u8])
}
