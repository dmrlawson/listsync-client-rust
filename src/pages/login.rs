use actix_web::{web, HttpRequest};
use serde::Serialize;
use typed_html::elements::p;
use typed_html::html;

use crate::actix_session_remember::Session;
use crate::credentials::Credentials;
use crate::data_holder::DataHolder;
use crate::domtree_responder::DOMTreeResponder;
use crate::lsc_error::LscError;
use crate::page::page;
use crate::urls::Urls;

#[derive(Serialize)]
struct TokenToDelete {
    token: String,
}

fn display_message(query_string: &str) -> Box<p<String>> {
    match query_string {
        "message=user_deleted" => {
            html!(<p class="login-message">"User deleted.  Please log in:"</p>)
        }
        "failed=true" => html!(
            <p id="failure">
                "Login failed or timed out - please try again:"
            </p>
        ),
        _ => html!(
            <p class="login-message">"Please log in:"</p>
        ),
    }
}

pub async fn get(req: HttpRequest, urls: Urls) -> DOMTreeResponder {
    page(
        "Log in",
        None,
        html!(
            <div>
                <div class="login-div">
                    {display_message(req.query_string())}
                    <form
                        action={
                            urls
                                .url_for_static(&req, "root")
                                .unwrap()
                                .to_string()
                        }
                        method="post"
                    >
                        <label for="username">"Username "</label>
                        <input
                            id="username"
                            name="username"
                            placeholder="Enter your username e.g. andyb"
                        />
                        <label for="password">"Password "</label>
                        <input
                            id="password"
                            name="password"
                            placeholder="Enter your password"
                            type="password"
                        />
                        <span id="remember-span">
                            <input
                                type="checkbox"
                                name="remember_me"
                                id="remember_me"
                            />
                            <label for="remember_me">"Remember me"</label>
                        </span>
                        <button>"Login"</button>
                    </form>
                    <p id="cookie-warning">
                        "This site uses a single authentication "
                        <a href="https://en.wikipedia.org/wiki/HTTP_cookie">
                            "cookie"
                        </a>
                        " to enable logging in.  It is not used to track any"
                        " information about you."
                    </p>
                </div>
                <div class="secondary-section log-in-link-div">
                    <span>
                        "New visitor? "
                        <a href={
                            urls
                                .url_for_static(&req, "signup")
                                .unwrap()
                                .to_string()
                        }>
                            "Sign up for free"
                        </a>
                        "."
                    </span>
                </div>
            </div>
        ),
        &req,
        &urls,
    )
    .into()
}

/// The user clicked Log out.
pub async fn post(
    req: HttpRequest,
    urls: Urls,
    session: Session,
    data_holder: web::Data<DataHolder>,
) -> Result<DOMTreeResponder, LscError> {
    match Credentials::extract(&req, &urls).await {
        Ok(credentials) => {
            session.purge();
            delete_token(&req, &urls, &data_holder, &credentials).await?;
            Ok(get(req, urls).await)
        }
        Err(e) => Err(e),
    }
}

async fn delete_token(
    req: &HttpRequest,
    urls: &Urls,
    data_holder: &DataHolder,
    credentials: &Credentials,
) -> Result<(), LscError> {
    let url = format!("/v1/login/{}", &credentials.username);
    data_holder
        .api
        .delete(
            &credentials,
            &url,
            Some(
                serde_json::to_value(TokenToDelete {
                    token: String::from(&credentials.token),
                })
                .expect("Unable to serialize TokenToDelete"),
            ),
        )
        .await
        .map(|_| ())
        .map_err(|e| LscError::from_api_error(req, urls, e))
}
