use actix_web::{http, web, FromRequest, HttpRequest, HttpResponse};
use serde_json::json;

use crate::credentials::Credentials;
use crate::data_holder::DataHolder;
use crate::lsc_error::LscError;
use crate::urls::Urls;

use serde::Deserialize;

#[derive(Deserialize)]
pub struct NewList {
    pub title: String,
}

#[derive(Deserialize)]
pub struct ListInfo {
    pub listid: String,
    pub title: String,
}

pub async fn post(
    req: HttpRequest,
    urls: Urls,
    new_list: web::Form<NewList>,
) -> Result<HttpResponse, LscError> {
    match Credentials::extract(&req, &urls).await {
        Ok(credentials) => {
            new_list_page(&req, &urls, credentials, &new_list).await
        }
        Err(e) => Err(e),
    }
}

async fn new_list_page(
    req: &HttpRequest,
    urls: &Urls,
    credentials: Credentials,
    new_list: &NewList,
) -> Result<HttpResponse, LscError> {
    let data_holder = web::Data::<DataHolder>::extract(&req).await.unwrap();
    let url = format!("/v1/list/{}", credentials.username);
    data_holder
        .api
        .post(
            &credentials,
            &url,
            json!({"title": String::from(&new_list.title)}),
        )
        .await
        .map_err(|e| LscError::from_api_error(req, urls, e))
        .and_then(|res| {
            serde_json::from_value::<ListInfo>(res)
                .map(|list_info| {
                    HttpResponse::Found()
                        .header(
                            http::header::LOCATION,
                            urls.url_for(
                                req,
                                "list",
                                vec![
                                    credentials.username,
                                    String::from(&list_info.listid),
                                ],
                            )
                            .unwrap()
                            .to_string(),
                        )
                        .finish()
                        .into_body()
                })
                .map_err(|_| {
                    LscError::internal_error(
                        req,
                        urls,
                        "Failed to parse API response while adding new list",
                    )
                })
        })
}
