use actix_web::{web, HttpRequest};
use serde::Deserialize;
use typed_html::html;

use crate::credentials::Credentials;
use crate::data_holder::DataHolder;
use crate::domtree_responder::DOMTreeResponder;
use crate::lsc_error::LscError;
use crate::page::page;
use crate::urls::Urls;

#[derive(Deserialize)]
pub struct Path {
    username: String,
    listid: String,
}

#[derive(Deserialize)]
pub struct ListInfo {
    pub listid: String,
    pub title: String,
}

pub async fn get(
    req: HttpRequest,
    urls: Urls,
    path: web::Path<Path>,
    data_holder: web::Data<DataHolder>,
) -> Result<DOMTreeResponder, LscError> {
    match Credentials::extract(&req, &urls).await {
        Ok(credentials) => {
            show_list_page(&req, &urls, &path, &data_holder, &credentials).await
        }
        Err(e) => Err(e),
    }
}

async fn show_list_page(
    req: &HttpRequest,
    urls: &Urls,
    path: &Path,
    data_holder: &DataHolder,
    credentials: &Credentials,
) -> Result<DOMTreeResponder, LscError> {
    let url = format!("/v1/list/{}/{}", path.username, path.listid);
    data_holder
        .api
        .get(&credentials, &url)
        .await
        .map_err(|e| LscError::from_api_error(req, urls, e))
        .and_then(|res| {
            display_list_properties(path, credentials, res, req, urls)
        })
}

fn display_list_properties(
    path: &Path,
    credentials: &Credentials,
    api_response: serde_json::Value,
    req: &HttpRequest,
    urls: &Urls,
) -> Result<DOMTreeResponder, LscError> {
    serde_json::from_value::<ListInfo>(api_response)
        .map_err(|_| {
            LscError::internal_error(req, urls, "Failed to parse ListInfo")
        })
        .map(|items| show_list_rename_html(path, credentials, items, req, urls))
}

fn show_list_rename_html(
    path: &Path,
    credentials: &Credentials,
    list_info: ListInfo,
    req: &HttpRequest,
    urls: &Urls,
) -> DOMTreeResponder {
    page(
        &format!("Rename {}", &list_info.title),
        Some(&credentials.username),
        html!(
            <div>
                <form
                    id="do-rename-list-form"
                    action={
                        urls.url_for(
                            req,
                            "list",
                            vec![&path.username, &list_info.listid]
                        ).unwrap().to_string()
                    }
                    method="post"
                >
                    <input type="text" name="title" value={&list_info.title}/>
                    <div id="rename-button">
                        <button>"Rename"</button>
                    </div>
                </form>
            </div>
        ),
        req,
        urls,
    )
    .into()
}
