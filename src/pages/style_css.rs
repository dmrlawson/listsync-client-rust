use actix_web::{http, HttpRequest, HttpResponse};

use crate::urls::Urls;

pub async fn get(req: HttpRequest, urls: Urls) -> HttpResponse {
    HttpResponse::Ok()
        .content_type("text/css")
        .header(http::header::CACHE_CONTROL, "max-age=2419200")
        .body(
            include_str!("style.css").replace(
                "{background_image}",
                &urls
                    .url_for_static(&req, "background_image")
                    .unwrap()
                    .to_string(),
            ),
        )
}
