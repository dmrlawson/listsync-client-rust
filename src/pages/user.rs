use actix_web::http::StatusCode;
use actix_web::{web, FromRequest, HttpRequest, HttpResponse, Responder};
use serde::Deserialize;
use serde_json::json;
use typed_html::dom::{DOMTree, UnsafeTextNode};
use typed_html::elements::div;
use typed_html::{html, text, unsafe_text};

use crate::credentials::Credentials;
use crate::data_holder::DataHolder;
use crate::domtree_responder::DOMTreeResponder;
use crate::lsc_error::LscError;
use crate::page::page;
use crate::urls::Urls;

#[derive(Deserialize)]
pub struct Path {
    username: String,
}

#[derive(Deserialize)]
struct ListInfo {
    listid: String,
    title: String,
}

#[derive(Deserialize)]
pub struct NewPassword {
    password: String,
    password_repeat: String,
}

///! Change password
pub async fn post(
    new_password: web::Form<NewPassword>,
    req: HttpRequest,
    urls: Urls,
    path: web::Path<Path>,
) -> Result<HttpResponse, LscError> {
    match Credentials::extract(&req, &urls).await {
        Ok(credentials) => {
            change_password_page(&new_password, &req, &urls, &path, credentials)
                .await
        }
        Err(e) => Err(e),
    }
}

async fn change_password_page(
    new_password: &NewPassword,
    req: &HttpRequest,
    urls: &Urls,
    path: &Path,
    credentials: Credentials,
) -> Result<HttpResponse, LscError> {
    if new_password.password != new_password.password_repeat {
        fail(
            req,
            urls,
            path,
            credentials,
            html!(<div id="failure">"Passwords didn't match!"</div>),
        )
        .await
    } else if new_password.password == "" {
        fail(
            req,
            urls,
            path,
            credentials,
            html!(<div id="failure">"Passwords can't be empty!"</div>),
        )
        .await
    } else {
        match change_password(new_password, req, urls, path, &credentials).await
        {
            Ok(_) => user_page(
                req,
                urls,
                path,
                credentials,
                Some(html!(<div id="message">"Password changed."</div>)),
            )
            .await
            .respond_to(req)
            .await
            .map_err(|_| {
                LscError::internal_error(
                    req,
                    urls,
                    "Failed to convert post user page to string",
                )
            }),
            Err(e) => Err(e),
        }
    }
}

async fn change_password(
    new_password: &NewPassword,
    req: &HttpRequest,
    urls: &Urls,
    path: &Path,
    credentials: &Credentials,
) -> Result<(), LscError> {
    let data_holder = web::Data::<DataHolder>::extract(&req).await.unwrap();
    let url = format!("/v1/user/{}", path.username);
    data_holder
        .api
        .put(
            &credentials,
            &url,
            json!({"password": &new_password.password}),
        )
        .await
        .map_err(|e| LscError::from_api_error(req, urls, e))
        .map(|_| ())
}

async fn fail(
    req: &HttpRequest,
    urls: &Urls,
    path: &Path,
    credentials: Credentials,
    extra_message: Box<div<String>>,
) -> Result<HttpResponse, LscError> {
    user_page(req, urls, path, credentials, Some(extra_message))
        .await
        .respond_to(req)
        .await
        .map_err(|_| {
            LscError::internal_error(
                req,
                urls,
                "Failed to convert user page to string",
            )
        })
        .map(|mut res| {
            *res.status_mut() = StatusCode::BAD_REQUEST;
            res
        })
}

pub async fn get(
    req: HttpRequest,
    urls: Urls,
    path: web::Path<Path>,
) -> Result<DOMTreeResponder, LscError> {
    match Credentials::extract(&req, &urls).await {
        Ok(credentials) => {
            user_page(&req, &urls, &path, credentials, None).await
        }
        Err(e) => Err(e),
    }
}

async fn user_page(
    req: &HttpRequest,
    urls: &Urls,
    path: &Path,
    credentials: Credentials,
    extra_message: Option<Box<div<String>>>,
) -> Result<DOMTreeResponder, LscError> {
    let data_holder = web::Data::<DataHolder>::extract(&req).await.unwrap();
    let url = format!("/v1/list/{}", path.username);
    data_holder
        .api
        .get(&credentials, &url)
        .await
        .map_err(|e| LscError::from_api_error(req, urls, e))
        .and_then(|res| {
            serde_json::from_value::<Vec<ListInfo>>(res).map_err(|_| {
                LscError::internal_error(
                    req,
                    urls,
                    "Failed to parse list of ListInfo",
                )
            })
        })
        .map(|list_infos| {
            user_page_html(
                req,
                urls,
                path,
                &credentials,
                list_infos,
                extra_message,
            )
        })
}

fn messages(query: &str) -> Option<DOMTree<String>> {
    match query {
        "message=list_deleted" => Some(html!(
            <div class="message">"List deleted."</div>
        )),
        "error=sure_required" => Some(html!(
            <div class="error">"You must choose \"I am sure\"."</div>
        )),
        _ => None,
    }
}

fn display_message(
    query: &str,
    extra_message: Option<Box<div<String>>>,
) -> Box<UnsafeTextNode<String>> {
    unsafe_text!(format!(
        "{}{}",
        messages(query)
            .map(|e| e.to_string())
            .unwrap_or(String::from("")),
        extra_message
            .map(|t| t.to_string())
            .unwrap_or(String::from(""))
    ))
}

fn unnamed_if_empty(title: &str) -> &str {
    if title.is_empty() {
        "(unnamed)"
    } else {
        title
    }
}

fn user_page_html(
    req: &HttpRequest,
    urls: &Urls,
    path: &Path,
    credentials: &Credentials,
    list_infos: Vec<ListInfo>,
    extra_message: Option<Box<div<String>>>,
) -> DOMTreeResponder {
    page(
        "My lists",
        Some(&credentials.username),
        html!(
            <div>
                {display_message(req.query_string(), extra_message)}
                <form
                    id="new-list-form"
                    action={
                        urls.url_for_static(req, "newlist").unwrap().to_string()
                    }
                    method="post"
                >
                    <label for="new-list-name">"Create a new list:"</label>
                    <input
                        type="text"
                        id="new-list-name"
                        name="title"
                        placeholder="Enter a name for the list"
                    />
                    <button>"Create"</button>
                </form>
                <div id="choose-list-div">
                    <p>"Choose a list:"</p>
                    <ul>{list_infos.iter().map(|list_info| html!(
                        <li>
                            <a href={
                                    urls.url_for(req,
                                        "list",
                                        vec![
                                            &path.username,
                                            &list_info.listid
                                        ]
                                    ).unwrap().to_string()
                            }>
                                {text!(
                                    "{}",
                                    unnamed_if_empty(&list_info.title)
                                )}
                            </a>
                        </li>
                    ))}</ul>
                </div>
                <div class="secondary-section">
                    <p>"Change my password"</p>
                    <form
                        id="change-password-form"
                        action={
                            urls.url_for(req,
                                "user",
                                vec![
                                    &path.username,
                                ]
                            ).unwrap().to_string()
                        }
                        method="post"
                    >
                        <label for="password">"Password "</label>
                        <input
                            id="password"
                            name="password"
                            placeholder="Enter a new password"
                            type="password"
                        />
                        <label for="password">
                            "Password (repeat) "
                        </label>
                        <input
                            id="password_repeat"
                            name="password_repeat"
                            placeholder="Repeat new password"
                            type="password"
                        />
                        <span><button>"Change password"</button></span>
                    </form>
                </div>
                <div class="secondary-section">
                    <p>"Delete this user"</p>
                    <form
                        id="delete-user-form"
                        class="secondary-section"
                        action={
                            urls.url_for(
                                req,
                                "deleteuser",
                                vec![&path.username]
                            ).unwrap().to_string()
                        }
                        method="post"
                    >
                        <button name="delete-user">
                            "Delete myself and all my lists"
                        </button>
                        <input
                            type="checkbox"
                            name="sure"
                            id="delete-user-sure"
                        />
                        <label for="delete-user-sure">"I am sure"</label>
                    </form>
                </div>
            </div>
        ),
        req,
        urls,
    )
    .into()
}
