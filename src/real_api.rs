use async_trait::async_trait;
use reqwest;

use crate::api::{Api, ApiError};
use crate::credentials::Credentials;

pub struct RealApi {
    server_url: String,
    reqwest_client: reqwest::Client,
}

impl RealApi {
    pub fn new(server_url: String) -> RealApi {
        RealApi {
            server_url,
            reqwest_client: reqwest::Client::new(),
        }
    }

    async fn request(
        &self,
        credentials: Option<&Credentials>,
        path: &str,
        method: reqwest::Method,
        body: Option<serde_json::Value>,
    ) -> Result<serde_json::Value, ApiError> {
        let url = format!("{}{}", self.server_url, path);
        let req = self.reqwest_client.request(method, &url);

        let req = if let Some(credentials) = credentials {
            req.basic_auth(&credentials.username, Some(&credentials.token))
        } else {
            req
        };

        let req = if let Some(body) = body {
            req.json(&body)
        } else {
            req
        };

        let res: Result<reqwest::Response, reqwest::Error> =
            req.send().await.and_then(|res| res.error_for_status());

        match res {
            Ok(res) => match res.status() {
                reqwest::StatusCode::NO_CONTENT => {
                    Ok(serde_json::Value::String(String::from("")))
                }
                _ => res.json().await,
            },
            Err(e) => Err(e),
        }
        .map_err(|e| {
            println!("ERROR: {:?}", e);
            // TODO: proper error logging
            e.into()
        })
    }
}

impl From<reqwest::Error> for ApiError {
    fn from(e: reqwest::Error) -> Self {
        e.status().map_or(ApiError::InternalError, |status_code| {
            match status_code {
                reqwest::StatusCode::UNAUTHORIZED => {
                    ApiError::AuthenticationFailed
                }
                reqwest::StatusCode::FORBIDDEN => ApiError::Forbidden,
                reqwest::StatusCode::CONFLICT => ApiError::UserAlreadyExists,
                _ => ApiError::InternalError,
            }
        })
    }
}

#[async_trait]
impl Api for RealApi {
    async fn get(
        &self,
        credentials: &Credentials,
        path: &str,
    ) -> Result<serde_json::Value, ApiError> {
        self.request(Some(credentials), path, reqwest::Method::GET, None)
            .await
    }

    async fn get_no_credentials(
        &self,
        path: &str,
    ) -> Result<serde_json::Value, ApiError> {
        self.request(None, path, reqwest::Method::GET, None).await
    }

    async fn put(
        &self,
        credentials: &Credentials,
        path: &str,
        body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        self.request(Some(credentials), path, reqwest::Method::PUT, Some(body))
            .await
    }

    async fn put_no_credentials(
        &self,
        path: &str,
        body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        self.request(None, path, reqwest::Method::PUT, Some(body))
            .await
    }

    async fn post(
        &self,
        credentials: &Credentials,
        path: &str,
        body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        self.request(Some(credentials), path, reqwest::Method::POST, Some(body))
            .await
    }

    async fn post_no_credentials(
        &self,
        path: &str,
        body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        self.request(None, path, reqwest::Method::POST, Some(body))
            .await
    }

    async fn delete(
        &self,
        credentials: &Credentials,
        path: &str,
        body: Option<serde_json::Value>,
    ) -> Result<serde_json::Value, ApiError> {
        self.request(Some(credentials), path, reqwest::Method::DELETE, body)
            .await
    }
}
