use actix_http::Payload;
use actix_web::error::UrlGenerationError;
use actix_web::{web, FromRequest, HttpRequest};
use futures::future;
use futures::future::{FutureExt, Map};
use url::Url;

use crate::data_holder::DataHolder;

#[derive(Clone)]
pub struct Urls {
    prefix: Option<String>,
}

impl Urls {
    pub fn new(prefix: Option<String>) -> Urls {
        Urls { prefix }
    }

    pub fn url_for<U, I>(
        &self,
        req: &HttpRequest,
        name: &str,
        elements: U,
    ) -> Result<Url, UrlGenerationError>
    where
        U: IntoIterator<Item = I>,
        I: AsRef<str>,
    {
        req.url_for(name, elements)
            .map(|url| self.prepend_prefix(url))
    }

    pub fn url_for_static(
        &self,
        req: &HttpRequest,
        name: &str,
    ) -> Result<Url, UrlGenerationError> {
        req.url_for_static(name).map(|url| self.prepend_prefix(url))
    }

    fn prepend_prefix(&self, mut url: Url) -> Url {
        if let Some(prefix) = &self.prefix {
            url.set_path(&format!("{}{}", prefix, url.path()))
        }
        url
    }
}

impl FromRequest for Urls {
    type Error = actix_web::Error; // We can't return an LscError since it
                                   // requires a Urls!
    type Future = Map<
        future::Ready<Result<web::Data<DataHolder>, actix_web::Error>>,
        Box<
            dyn FnOnce(
                Result<web::Data<DataHolder>, actix_web::Error>,
            ) -> Result<Self, Self::Error>,
        >,
    >;

    type Config = ();

    fn from_request(
        req: &HttpRequest,
        payload: &mut Payload,
    ) -> <Self as FromRequest>::Future {
        web::Data::<DataHolder>::from_request(req, payload).map(Box::new(
            |res_data_holder: Result<
                web::Data<DataHolder>,
                actix_web::Error,
            >| {
                res_data_holder.map(|data_holder| data_holder.urls.clone())
            },
        ))
    }
}
