use async_trait::async_trait;
use regex::Regex;
use serde::{Deserialize, Serialize};
use serde_json::json;
use std::collections::HashMap;
use std::sync::{Arc, Mutex};

use listsync_client_rust::api::{Api, ApiError};
use listsync_client_rust::credentials::Credentials;

#[derive(Clone, Deserialize, Serialize)]
pub struct ListItem {
    pub itemid: String,
    pub order: f64,
    pub ticked: bool,
    pub text: String,
}

#[derive(Deserialize)]
struct NewList {
    title: String,
}

#[derive(Deserialize)]
struct RegistrationCode {
    registration_code: String,
}

#[derive(Deserialize)]
struct NewUser {
    username: String,
    password: String,
}

struct List {
    listid: String,
    title: String,
    items: Vec<ListItem>,
}

#[derive(Serialize)]
struct ListInfo {
    listid: String,
    title: String,
}

impl List {
    fn items(&self) -> serde_json::Value {
        serde_json::to_value(&self.items).unwrap()
    }

    fn info(&self) -> serde_json::Value {
        serde_json::to_value(ListInfo {
            listid: String::from(&self.listid),
            title: String::from(&self.title),
        })
        .unwrap()
    }

    fn replace_items(&mut self, items: Vec<ListItem>) -> serde_json::Value {
        self.items = items;
        self.items()
    }
}

struct User {
    username: String,
    password: String,
    lists: HashMap<String, List>,
    remember_me: Option<bool>,
    nid: u32,
    deleted_tokens: Vec<String>,
    registered: bool,
}

impl User {
    fn change_password(
        &mut self,
        body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        body.get("password")
            .ok_or(ApiError::InternalError)
            .and_then(|j| j.as_str().ok_or(ApiError::InternalError))
            .map(|pw| {
                self.password = String::from(pw);
                json!({"notused?":"notused"})
            })
    }

    fn lists(&self) -> serde_json::Value {
        let mut lists: Vec<&List> = self.lists.values().collect();
        lists.sort_by_key(|list| &list.listid);
        serde_json::Value::Array(
            lists
                .iter()
                .map(|list| {
                    json!(
                        {
                            "listid": list.listid,
                            "title": list.title
                        }
                    )
                })
                .collect(),
        )
    }

    fn add_list(
        &mut self,
        body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        let list: NewList =
            serde_json::from_value(body).expect("Unable to parse NewList");

        let id = self.next_id();
        self.lists.insert(
            String::from(&id),
            List {
                listid: String::from(&id),
                title: String::from(&list.title),
                items: Vec::new(),
            },
        );
        Ok(json!({"listid": id, "title": list.title}))
    }

    fn update_list(
        &mut self,
        listid: &str,
        body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        let updated_list: NewList = serde_json::from_value(body)
            .expect("Failed to parse list update value");
        let list = self.lists.get_mut(listid).ok_or(ApiError::InternalError)?;
        list.title = updated_list.title;
        Ok(serde_json::to_value(ListInfo {
            listid: String::from(listid),
            title: String::from(&list.title),
        })
        .expect("Failed to serlialize ListInfo"))
    }

    fn delete_list(
        &mut self,
        listid: &str,
    ) -> Result<serde_json::Value, ApiError> {
        match self.lists.remove(listid) {
            Some(_) => Ok(serde_json::Value::String(String::from(""))),
            None => Err(ApiError::InternalError),
        }
        // The error could be a NotFound error - the list was not found,
        // but the fact that the list is not found is kind of InternalError,
        // because the UI should not have offered you a way to delete it?
    }

    fn delete_token(
        &mut self,
        token_owner_username: &str,
        token: &str,
    ) -> Result<serde_json::Value, ApiError> {
        if token_owner_username != self.username
            || token != format!("{}-token", self.username)
        {
            Err(ApiError::InternalError)
        } else {
            self.deleted_tokens.push(String::from(token));
            Ok(json!({}))
        }
    }

    fn replace_items(
        &mut self,
        listid: &str,
        body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        let items: Vec<ListItem> = serde_json::from_value(body)
            .map_err(|_| ApiError::InternalError)?;
        self.lists
            .get_mut(listid)
            .ok_or(ApiError::InternalError)
            .map(|list| list.replace_items(items))
    }

    fn next_id(&mut self) -> String {
        self.nid += 1;
        format!("id{}", self.nid)
    }
}

enum PathType {
    User,
    UserList,
    List(String),
    ListItems(String),
    Login,
    LoginUser(String),
}

impl PathType {
    fn parse(username: &str, path: &str) -> Option<PathType> {
        let items_re =
            Regex::new(&format!(r"^/v1/list/{}/([^/]*)/items$", username))
                .unwrap();

        let list_re =
            Regex::new(&format!(r"^/v1/list/{}/([^/]*)$", username)).unwrap();

        let login_re = Regex::new(r"^/v1/login/([^/]*)$").unwrap();

        if path == "/v1/login" {
            Some(PathType::Login)
        } else if let Some(captures) = login_re.captures(path) {
            Some(PathType::LoginUser(String::from(
                captures.get(1).unwrap().as_str(),
            )))
        } else if path == format!("/v1/list/{}", username) {
            Some(PathType::UserList)
        } else if path == format!("/v1/user/{}", username) {
            Some(PathType::User)
        } else if let Some(captures) = items_re.captures(path) {
            Some(PathType::ListItems(String::from(
                captures.get(1).unwrap().as_str(),
            )))
        } else if let Some(captures) = list_re.captures(path) {
            Some(PathType::List(String::from(
                captures.get(1).unwrap().as_str(),
            )))
        } else {
            None
        }
    }
}

enum NoCredPathType {
    NewUser,
    Privacy,
    UpdateUser(String),
}

impl NoCredPathType {
    fn parse(path: &str) -> Option<NoCredPathType> {
        let user_re = Regex::new(r"^/v1/newuser/([^/]*)$").unwrap();

        if path == "/v1/newuser" {
            Some(NoCredPathType::NewUser)
        } else if let Some(captures) = user_re.captures(path) {
            Some(NoCredPathType::UpdateUser(String::from(
                captures.get(1).unwrap().as_str(),
            )))
        } else if path == "/v1/privacy" {
            Some(NoCredPathType::Privacy)
        } else {
            None
        }
    }
}

#[derive(Clone)]
pub struct FakeApi {
    users: Arc<Mutex<HashMap<String, User>>>,
}

impl FakeApi {
    pub fn new() -> FakeApi {
        FakeApi {
            users: Arc::new(Mutex::new(HashMap::new())),
        }
    }

    pub fn add_user(&mut self, username: &str, password: &str) {
        self.users.lock().unwrap().insert(
            String::from(username),
            User {
                username: String::from(username),
                password: String::from(password),
                lists: HashMap::new(),
                remember_me: None,
                nid: 0,
                deleted_tokens: vec![],
                registered: true,
            },
        );
    }

    fn delete_user(
        &self,
        username: &str,
    ) -> Result<serde_json::Value, ApiError> {
        self.users
            .lock()
            .unwrap()
            .remove(username)
            .map(|_| json!({"unused":"unused?"}))
            .ok_or(ApiError::InternalError)
    }

    pub fn get_user_reg_code(
        &self,
        _username: &str,
    ) -> Result<serde_json::Value, ApiError> {
        Ok(json!({"registration_code":"reg1"}))
    }

    pub fn register_user(
        &self,
        username: &str,
        details: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        // TODO: track whether a user is registered and return an error
        //       here if so?
        let registration_code: RegistrationCode =
            serde_json::from_value(details)
                .expect("Unable to parse RegistrationCode");
        self.users
            .lock()
            .unwrap()
            .get_mut(username)
            .ok_or(ApiError::InternalError)
            .and_then(|mut user| {
                if registration_code.registration_code == "reg1" {
                    user.registered = true;
                    Ok(json!({}))
                } else {
                    Err(ApiError::InternalError)
                }
            })
    }

    pub fn add_unregistered_user(
        &self,
        details: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        let user_details: NewUser =
            serde_json::from_value(details).expect("Unable to parse NewUser");
        let mut users = self.users.lock().unwrap();

        if users.contains_key(&user_details.username) {
            return Err(ApiError::UserAlreadyExists);
        }

        users.insert(
            String::from(&user_details.username),
            User {
                username: String::from(&user_details.username),
                password: String::from(&user_details.password),
                lists: HashMap::new(),
                remember_me: None,
                nid: 0,
                deleted_tokens: vec![],
                registered: false,
            },
        );

        Ok(serde_json::to_value(
            vec![
                ("username", user_details.username.as_str()),
                ("registration_code", "reg1"),
            ]
            .iter()
            .cloned()
            .collect::<HashMap<&str, &str>>(),
        )
        .expect("Failed to serialize unregistered user"))
    }

    pub fn get_remember_me(&self, username: &str) -> Option<bool> {
        self.users
            .lock()
            .unwrap()
            .get(username)
            .expect("User not found.")
            .remember_me
    }

    pub fn get_deleted_tokens(&self, username: &str) -> Vec<String> {
        self.users
            .lock()
            .unwrap()
            .get(username)
            .expect("User not found.")
            .deleted_tokens
            .clone()
    }

    fn check_credentials<F>(
        &self,
        credentials: &Credentials,
        f: F,
    ) -> Result<serde_json::Value, ApiError>
    where
        F: FnOnce(&mut User) -> Result<serde_json::Value, ApiError>,
    {
        self.users
            .lock()
            .unwrap()
            .get_mut(&credentials.username)
            .ok_or(ApiError::AuthenticationFailed)
            .and_then(|user| {
                if credentials.token
                    == format!("{}-token", credentials.username)
                    || credentials.token == user.password
                {
                    Ok(user)
                } else {
                    Err(ApiError::AuthenticationFailed)
                }
            })
            .and_then(f)
    }
}

#[async_trait]
impl Api for FakeApi {
    async fn get(
        &self,
        credentials: &Credentials,
        path: &str,
    ) -> Result<serde_json::Value, ApiError> {
        self.check_credentials(credentials, |user| {
            match PathType::parse(&user.username, path) {
                Some(PathType::UserList) => Ok(user.lists()),
                Some(PathType::ListItems(listid)) => user
                    .lists
                    .get(&listid)
                    .map(|list| list.items())
                    .ok_or(ApiError::InternalError),
                Some(PathType::List(listid)) => user
                    .lists
                    .get(&listid)
                    .map(|list| list.info())
                    .ok_or(ApiError::InternalError),
                Some(PathType::Login) => Err(ApiError::InternalError),
                Some(PathType::LoginUser(_)) => Err(ApiError::InternalError),
                Some(PathType::User) => Err(ApiError::InternalError),
                None => Err(ApiError::InternalError),
            }
        })
    }

    async fn get_no_credentials(
        &self,
        path: &str,
    ) -> Result<serde_json::Value, ApiError> {
        match NoCredPathType::parse(path) {
            Some(NoCredPathType::NewUser) => Err(ApiError::InternalError),
            Some(NoCredPathType::Privacy) => Ok(json!("PRIV<p>acy</p>")),
            Some(NoCredPathType::UpdateUser(username)) => {
                self.get_user_reg_code(&username)
            }
            None => Err(ApiError::InternalError),
        }
    }

    async fn put(
        &self,
        credentials: &Credentials,
        path: &str,
        body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        self.check_credentials(credentials, |user| {
            match PathType::parse(&user.username, path) {
                Some(PathType::UserList) => Err(ApiError::InternalError),
                Some(PathType::ListItems(listid)) => {
                    user.replace_items(&listid, body)
                }
                Some(PathType::List(listid)) => user.update_list(&listid, body),
                Some(PathType::Login) => Err(ApiError::InternalError),
                Some(PathType::LoginUser(_)) => Err(ApiError::InternalError),
                Some(PathType::User) => user.change_password(body),
                None => Err(ApiError::InternalError),
            }
        })
    }

    async fn put_no_credentials(
        &self,
        path: &str,
        body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        match NoCredPathType::parse(path) {
            Some(NoCredPathType::NewUser) => Err(ApiError::InternalError),
            Some(NoCredPathType::Privacy) => Err(ApiError::InternalError),
            Some(NoCredPathType::UpdateUser(username)) => {
                self.register_user(&username, body)
            }
            None => Err(ApiError::InternalError),
        }
    }

    async fn post(
        &self,
        credentials: &Credentials,
        path: &str,
        body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        self.check_credentials(credentials, |user| {
            match PathType::parse(&user.username, path) {
                Some(PathType::UserList) => user.add_list(body),
                Some(PathType::ListItems(_listid)) => {
                    Err(ApiError::InternalError)
                }
                Some(PathType::List(_)) => Err(ApiError::InternalError),
                Some(PathType::Login) => {
                    if user.registered {
                        user.remember_me = Some(
                            body.get("remember_me").unwrap().as_bool().unwrap(),
                        );
                        Ok(json!(
                            {
                                "username": String::from(&user.username),
                                "token": format!("{}-token", &user.username),
                            }
                        ))
                    } else {
                        Err(ApiError::Forbidden)
                    }
                }
                Some(PathType::LoginUser(_)) => Err(ApiError::InternalError),
                Some(PathType::User) => Err(ApiError::InternalError),
                None => Err(ApiError::InternalError),
            }
        })
    }

    async fn post_no_credentials(
        &self,
        path: &str,
        body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        match NoCredPathType::parse(path) {
            Some(NoCredPathType::NewUser) => self.add_unregistered_user(body),
            Some(NoCredPathType::Privacy) => Err(ApiError::InternalError),
            Some(NoCredPathType::UpdateUser(_)) => Err(ApiError::InternalError),
            None => Err(ApiError::InternalError),
        }
    }

    async fn delete(
        &self,
        credentials: &Credentials,
        path: &str,
        body: Option<serde_json::Value>,
    ) -> Result<serde_json::Value, ApiError> {
        let mut delete_user = false;
        let ret = self.check_credentials(credentials, |user| {
            match PathType::parse(&user.username, path) {
                Some(PathType::UserList) => Err(ApiError::InternalError),
                Some(PathType::ListItems(_)) => Err(ApiError::InternalError),
                Some(PathType::List(listid)) => user.delete_list(&listid),
                Some(PathType::Login) => Err(ApiError::InternalError),
                Some(PathType::LoginUser(username)) => user.delete_token(
                    &username,
                    &body.expect("No request body supplied when deleting a token.").get("token").expect(
                        "No 'token' property in JSON body of delete request.",
                    ).as_str().expect("'token' property was not a string."),
                ),
                Some(PathType::User) => {delete_user = true; Err(ApiError::InternalError)},
                None => Err(ApiError::InternalError),
            }
        });
        if delete_user {
            // check_credentials has a lock on users, so we must do
            // this outside.
            self.delete_user(&credentials.username)
        } else {
            ret
        }
    }
}
