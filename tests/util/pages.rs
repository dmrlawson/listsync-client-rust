use typed_html::dom::DOMTree;
use typed_html::{html, text};

use crate::util::string_repl::StringRepl;

fn pfx(tmpl: &str, prefix: &str) -> String {
    if prefix.is_empty() {
        tmpl.replace("{}", "")
    } else {
        tmpl.replace("{}", &format!("{}/", prefix))
    }
}

fn page_template(prefix: &str) -> String {
    let main: DOMTree<String> = html!(
        <html>
            <head>
                <title>"{title}"</title>
                <meta charset="utf-8"/>
                <meta
                    name="viewport"
                    content="width=device-width,initial-scale=1"
                />
                <link
                    href={pfx("http://localhost:8080/{}style.css", prefix)}
                    rel="stylesheet"
                />
            </head>
            <body>
                "{body}"
                <footer>
                    <p>
                        <a href=
                            "https://gitlab.com/listsync/listsync-client-rust"
                        >
                            "listsync HTML"
                        </a>
                        " is Free Software, released under the AGPLv3."
                    </p>
                    <p>"Find out how to "
                        <a
                            href={
                                pfx("http://localhost:8080/{}contact", prefix)
                            }
                        >
                            "contact the server admins"
                        </a>
                        " or "
                        <a href={
                                pfx("http://localhost:8080/{}privacy", prefix)
                            }>
                            "read the privacy policy"
                        </a>
                        "."
                    </p>
                </footer>
            </body>
        </html>
    );

    format!("<!DOCTYPE html>\n{}", main.to_string())
}

fn login_body(message: DOMTree<String>) -> String {
    let tmpl: DOMTree<String> = html!(
        <div id="whole-page-div">
            <nav id="plain-header-nav">
                <a href="http://localhost:8080/">"\u{1F3E0} Home"</a>
            </nav>
            <div>
                <div class="login-div">
                    "{message}{form}{cookies}"
                </div>
                <div class="secondary-section log-in-link-div">
                    <span>
                        "New visitor? "
                        <a href="http://localhost:8080/signup">
                            "Sign up for free"
                        </a>
                        "."
                    </span>
                </div>
            </div>
        </div>
    );
    tmpl.to_string()
        .repl("{message}", message)
        .repl(
            "{form}",
            html!(
                <form action="http://localhost:8080/" method="post">
                    <label for="username">"Username "</label>
                    <input
                        id="username"
                        name="username"
                        placeholder="Enter your username e.g. andyb"
                    />
                    <label for="password">"Password "</label>
                    <input
                        id="password"
                        name="password"
                        placeholder="Enter your password"
                        type="password"
                    />
                    <span id="remember-span">
                        <input
                            type="checkbox"
                            name="remember_me"
                            id="remember_me"
                        />
                        <label for="remember_me">"Remember me"</label>
                    </span>
                    <button>"Login"</button>
                </form>
            ),
        )
        .repl(
            "{cookies}",
            html!(
                <p id="cookie-warning">
                    "This site uses a single authentication "
                    <a href="https://en.wikipedia.org/wiki/HTTP_cookie">
                        "cookie"
                    </a>
                    " to enable logging in.  It is not used to track any"
                    " information about you."
                </p>
            ),
        )
}

fn login_page(message: DOMTree<String>) -> String {
    page_template("")
        .replace("{title}", "Log in - listsync HTML")
        .replace("{body}", &login_body(message))
}

pub fn login_page_first_time() -> String {
    login_page(html!(<p class="login-message">"Please log in:"</p>))
}

pub fn login_page_failed() -> String {
    login_page(html!(
        <p id="failure">"Login failed or timed out - please try again:"</p>
    ))
}

fn signup_body(message: DOMTree<String>) -> String {
    let tmpl: DOMTree<String> = html!(
        <div id="whole-page-div">
            <nav id="plain-header-nav">
                <a href="http://localhost:8080/">"\u{1F3E0} Home"</a>
            </nav>
            <div>
                <div class="login-div">
                    "{message}{form}{cookies}"
                </div>
                <div class="secondary-section log-in-link-div">
                    <span>
                        "Been here before? "
                        <a href="http://localhost:8080/login">
                            "Log in"
                        </a>
                        "."
                    </span>
                </div>
            </div>
        </div>
    );
    tmpl.to_string()
        .repl("{message}", message)
        .repl(
            "{form}",
            html!(
                <form action="http://localhost:8080/signup" method="post">
                    <label for="username">"Username "</label>
                    <input
                        id="username"
                        name="username"
                        placeholder="Enter a username e.g. andyb"
                    />
                    <label for="password">"Password "</label>
                    <input
                        id="password"
                        name="password"
                        placeholder="Enter a password"
                        type="password"
                    />
                    <label for="password">"Password (repeat) "</label>
                    <input
                        id="password_repeat"
                        name="password_repeat"
                        placeholder="Repeat your password"
                        type="password"
                    />
                    <button>"Sign up"</button>
                </form>
            ),
        )
        .repl(
            "{cookies}",
            html!(
                <p id="cookie-warning">
                    "This site uses a single authentication "
                    <a href="https://en.wikipedia.org/wiki/HTTP_cookie">
                        "cookie"
                    </a>
                    " to enable logging in.  It is not used to track any"
                    " information about you."
                </p>
            ),
        )
}

fn signup_page(message: DOMTree<String>) -> String {
    page_template("")
        .replace("{title}", "Sign up - listsync HTML")
        .replace("{body}", &signup_body(message))
}

pub fn signup_page_first_time() -> String {
    signup_page(
        html!(<p class="login-message">"Sign up to start making lists!"</p>),
    )
}

pub fn signup_page_failure() -> String {
    signup_page(html!(
        <p id="failure">
        "The passwords you typed do not match.  Please try again."
        </p>
    ))
}

pub fn user_exists_page() -> String {
    signup_page(html!(
        <p id="failure">
        "Someone has already taken that username.  Please try another."
        </p>
    ))
}

fn enter_reg_body(
    username: &str,
    reg_code: &str,
    message: DOMTree<String>,
) -> String {
    let tmpl: DOMTree<String> = html!(
        <div id="whole-page-div">
            <nav id="plain-header-nav">
                <a href="http://localhost:8080/">"\u{1F3E0} Home"</a>
            </nav>
            <div>
                <div class="login-div">
                    "{message}"
                    <p class="login-message">
                        "Your registration code is "
                        <span id="reg-code">{text!(reg_code)}</span>
                        ".  Please enter it below."
                    </p>
                    "{form}"
                </div>
            </div>
        </div>
    );
    tmpl.to_string().repl("{message}", message).repl(
        "{form}",
        html!(
            <form
                action={&format!("http://localhost:8080/signup/{}", username)}
                method="post"
            >
                <label for="registration_code">"Registration code "</label>
                <input
                    id="registration_code"
                    name="registration_code"
                    placeholder="Enter the code e.g. dF-9H"
                />
                <input
                    id="real_registration_code"
                    name="real_registration_code"
                    type="hidden"
                    value="reg1"
                />
                <button>"Submit"</button>
            </form>
        ),
    )
}

pub fn enter_reg_page(username: &str, reg_code: &str) -> String {
    page_template("")
        .replace("{title}", "Enter registration code - listsync HTML")
        .replace(
            "{body}",
            &enter_reg_body(username, reg_code, html!(<div></div>)),
        )
}

pub fn enter_reg_page_failure(username: &str, reg_code: &str) -> String {
    page_template("")
        .replace("{title}", "Enter registration code - listsync HTML")
        .replace(
            "{body}",
            &enter_reg_body(
                username,
                reg_code,
                html!(
                    <div id="failure">
                        "The registration code you typed was incorrect.  "
                        "Please try again."
                    </div>
                ),
            ),
        )
}

pub fn enter_reg_page_must_register(username: &str, reg_code: &str) -> String {
    page_template("")
        .replace("{title}", "Enter registration code - listsync HTML")
        .replace(
            "{body}",
            &enter_reg_body(
                username,
                reg_code,
                html!(
                    <div id="failure">
                        "You must register before you can log in."
                    </div>
                ),
            ),
        )
}

fn registered_page_body() -> String {
    let tmpl: DOMTree<String> = html!(
        <div id="whole-page-div">
            <nav id="plain-header-nav">
                <a href="http://localhost:8080/">"\u{1F3E0} Home"</a>
            </nav>
            <p class="log-in-link-div"><span>
                "Registration succeeded.  "
                <a href="http://localhost:8080/login">"Log in"</a>
                " to get started!"
            </span></p>
        </div>
    );
    tmpl.to_string()
}

pub fn registered_page() -> String {
    page_template("")
        .replace("{title}", "Registration succeeded - listsync HTML")
        .replace("{body}", &registered_page_body())
}

fn header(username: &str) -> DOMTree<String> {
    let home_url = format!("http://localhost:8080/list/{}", username);
    html!(
        <nav id="user-header-nav">
            <span id="home-span"><a href=&home_url>"\u{1F3E0} Home"</a></span>
            <form
                id="log-out-form"
                action="http://localhost:8080/login"
                method="post"
            >
                <button name="log_out">"Log out"</button>
            </form>
            <a href=&home_url>{text!("\u{1F464} {}", username)}</a>
        </nav>
    )
}

pub fn user_page(
    username: &str,
    listids: Vec<(&'static str, &'static str)>,
) -> String {
    page_template("")
        .replace("{title}", "My lists - listsync HTML")
        .repl(
            "{body}",
            html!(
                <div id="whole-page-div">
                    "{header}"
                    <div>
                        <form
                            id="new-list-form"
                            action="http://localhost:8080/newlist"
                            method="post"
                        >
                            <label for="new-list-name">
                                "Create a new list:"
                            </label>
                            <input
                                type="text"
                                id="new-list-name"
                                name="title"
                                placeholder="Enter a name for the list"
                            />
                            <button>"Create"</button>
                        </form>
                        <div id="choose-list-div">
                            <p>"Choose a list:"</p>
                            <ul>{
                                listids.iter().map(|(listid, title)|
                                    html!(
                                        <li><a href={
                                            format!(
                                                "http://localhost:8080\
                                                /list/{}/{}",
                                                username,
                                                listid
                                            )
                                        }>{
                                            text!("{}", title)
                                        }</a></li>
                                    )
                                )
                            }</ul>
                        </div>
                        <div class="secondary-section">
                            <p>"Change my password"</p>
                            <form
                                id="change-password-form"
                                action={
                                    &format!(
                                        "http://localhost:8080/list/{}",
                                        username
                                    )
                                }
                                method="post"
                            >
                                <label for="password">"Password "</label>
                                <input
                                    id="password"
                                    name="password"
                                    placeholder="Enter a new password"
                                    type="password"
                                />
                                <label for="password">
                                    "Password (repeat) "
                                </label>
                                <input
                                    id="password_repeat"
                                    name="password_repeat"
                                    placeholder="Repeat new password"
                                    type="password"
                                />
                                <span><button>"Change password"</button></span>
                            </form>
                        </div>
                        <div class="secondary-section">
                            <p>"Delete this user"</p>
                            <form
                                id="delete-user-form"
                                class="secondary-section"
                                action={
                                    &format!(
                                        "http://localhost:8080/deleteuser/{}",
                                        username
                                    )
                                }
                                method="post"
                            >
                                <button name="delete-user">
                                    "Delete myself and all my lists"
                                </button>
                                <input
                                    type="checkbox"
                                    name="sure"
                                    id="delete-user-sure"
                                />
                                <label for="delete-user-sure">"I am sure"</label>
                            </form>
                        </div>
                    </div>
                </div>
            ),
        )
        .repl("{header}", header(username))
}

pub fn list_page(
    title: &str,
    username: &str,
    message: DOMTree<String>,
) -> String {
    page_template("")
        .replace("{title}", &format!("{} - listsync HTML", title))
        .repl("{body}", message)
        .repl("{header}", header(username))
}

pub fn rename_list_page(
    title: &str,
    prefix: &str,
    username: &str,
    message: DOMTree<String>,
) -> String {
    page_template(prefix)
        .replace("{title}", &format!("Rename {} - listsync HTML", title))
        .repl("{body}", message)
        .repl("{header}", header(username))
}

pub fn contact_page() -> String {
    page_template("")
        .replace("{title}", "Contact - listsync HTML")
        .repl(
            "{body}",
            html!(
                <div id="whole-page-div">
                    <nav id="plain-header-nav">
                        <a href="http://localhost:8080/">"\u{1F3E0} Home"</a>
                    </nav>
                    <div class="info-container-div">
                        <h1>"Contact"</h1>
                        <div class="info-div">
                            <p>
                                "email "
                                <a href="mailto:me@example.com">"me"</a>
                                "."
                            </p>
                        </div>
                    </div>
                </div>
            ),
        )
}

pub fn privacy_page() -> String {
    page_template("")
        .replace("{title}", "Privacy - listsync HTML")
        .repl(
            "{body}",
            html!(
                <div id="whole-page-div">
                    <nav id="plain-header-nav">
                        <a href="http://localhost:8080/">"\u{1F3E0} Home"</a>
                    </nav>
                    <div class="info-container-div">
                        <h1>"Privacy"</h1>
                        <div class="info-div">
                            "PRIV"
                            <p>"acy"</p>
                        </div>
                    </div>
                </div>
            ),
        )
}

pub fn delete_user_page(username: &str) -> String {
    page_template("")
        .replace("{title}", "Delete user? - listsync HTML")
        .repl(
            "{body}",
            html!(
                <div id="whole-page-div">
                    "{header}"
                    <div>
                        <div class="error">
                            r#"Confirm by choosing "I am REALLY sure"."#
                        </div>
                        <div class="secondary-section">
                            <p>{text!("Delete user {}?", username)}</p>
                            <form
                                id="delete-user-form"
                                class="secondary-section"
                                action={
                                    &format!(
                                        "http://localhost:8080/deleteuser/{}",
                                        username
                                    )
                                }
                                method="post"
                            >
                                <button name="delete-user">
                                    "Delete myself and all my lists"
                                </button>
                                <input type="hidden" name="sure" value="on"/>
                                <input
                                    type="checkbox"
                                    name="really_sure"
                                    id="really_sure"
                                />
                                <label for="really_sure">
                                    "I am REALLY sure"
                                </label>
                            </form>
                        </div>
                    </div>
                </div>
            ),
        )
        .repl("{header}", header(username))
}
