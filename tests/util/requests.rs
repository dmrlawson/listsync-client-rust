use actix_http::body::Body;
use actix_http::{Request, Response};
use actix_web::dev::{Service, ServiceResponse};
use actix_web::http::Cookie;
use actix_web::web::{Bytes, BytesMut, HttpResponse};
use actix_web::{test, App, Error};
use futures::executor;
use futures::stream::StreamExt;

use listsync_client_rust::api::Api;
use listsync_client_rust::contact_page::ContactPage;
use listsync_client_rust::data_holder::DataHolder;
use listsync_client_rust::urls::Urls;

use crate::util::fake_api::FakeApi;

pub fn new_app(
    api: Box<dyn Api>,
) -> impl Service<Request = Request, Response = ServiceResponse, Error = Error>
{
    new_app_all(api, None)
}

pub fn new_app_with_prefix(
    api: Box<dyn Api>,
    prefix: String,
) -> impl Service<Request = Request, Response = ServiceResponse, Error = Error>
{
    new_app_all(api, Some(prefix))
}

pub fn new_app_all(
    api: Box<dyn Api>,
    prefix: Option<String>,
) -> impl Service<Request = Request, Response = ServiceResponse, Error = Error>
{
    executor::block_on(test::init_service(
        App::new()
            .data(DataHolder::new(
                api,
                ContactPage::new(String::from(
                    r#"<p>email <a href="mailto:me@example.com">me</a>.</p>"#,
                )),
                Urls::new(prefix),
            ))
            .wrap(listsync_client_rust::cookie_session::new_session())
            .configure(listsync_client_rust::config),
    ))
}

pub fn logged_in_as(
    username: &str,
) -> (
    impl Service<Request = Request, Response = ServiceResponse, Error = Error>,
    Cookie,
) {
    let mut api = Box::new(FakeApi::new());
    api.add_user(username, "pw");
    let mut app = new_app(api);
    let resp = post_form_no_cookie(
        &mut app,
        "/",
        &format!("username={}&password=pw", username),
    );
    (app, resp.cookies().next().unwrap().into_owned())
}

pub fn logged_in_with_api(
    username: &str,
    mut api: Box<FakeApi>,
) -> (
    impl Service<Request = Request, Response = ServiceResponse, Error = Error>,
    Cookie,
) {
    api.add_user(username, "pw");
    let mut app = new_app(api);
    let resp = post_form_no_cookie(
        &mut app,
        "/",
        &format!("username={}&password=pw", username),
    );
    (app, resp.cookies().next().unwrap().into_owned())
}

pub fn logged_in_with_prefix(
    username: &str,
    prefix: String,
) -> (
    impl Service<Request = Request, Response = ServiceResponse, Error = Error>,
    Cookie,
) {
    let mut api = Box::new(FakeApi::new());
    api.add_user(username, "pw");
    let mut app = new_app_with_prefix(api, prefix);
    let resp = post_form_no_cookie(
        &mut app,
        "/",
        &format!("username={}&password=pw", username),
    );
    (app, resp.cookies().next().unwrap().into_owned())
}

pub fn req<'a, S>(
    app: &'a mut S,
    req_type: test::TestRequest,
    path: &str,
) -> HttpResponse
where
    S: Service<
        Request = Request,
        Response = ServiceResponse<Body>,
        Error = Error,
    >,
{
    let req = req_type.uri(path);

    executor::block_on(app.call(req.to_request()))
        .map(|sr| sr.into())
        .unwrap_or_else(|e| HttpResponse::from_error(e))
}

pub fn get<'a, S>(
    app: &'a mut S,
    auth_cookie: &Cookie,
    path: &str,
) -> HttpResponse
where
    S: Service<
        Request = Request,
        Response = ServiceResponse<Body>,
        Error = Error,
    >,
{
    req(
        app,
        test::TestRequest::get().cookie(auth_cookie.clone()),
        path,
    )
}

pub fn post_form<'a, S>(
    app: &'a mut S,
    auth_cookie: &Cookie,
    path: &str,
    payload: &str,
) -> HttpResponse
where
    S: Service<
        Request = Request,
        Response = ServiceResponse<Body>,
        Error = Error,
    >,
{
    req(
        app,
        test::TestRequest::post()
            .cookie(auth_cookie.clone())
            .header("Content-Type", "application/x-www-form-urlencoded")
            .set_payload(Bytes::from(String::from(payload))),
        path,
    )
}

pub fn get_nocookie<'a, S>(app: &'a mut S, path: &str) -> HttpResponse
where
    S: Service<
        Request = Request,
        Response = ServiceResponse<Body>,
        Error = Error,
    >,
{
    req(app, test::TestRequest::get(), path)
}

pub fn post_form_no_cookie<'a, S>(
    app: &'a mut S,
    path: &str,
    payload: &str,
) -> HttpResponse
where
    S: Service<
        Request = Request,
        Response = ServiceResponse<Body>,
        Error = Error,
    >,
{
    req(
        app,
        test::TestRequest::post()
            .header("Content-Type", "application/x-www-form-urlencoded")
            .set_payload(Bytes::from(String::from(payload))),
        path,
    )
}

pub fn assert_contains_header(
    resp: &HttpResponse,
    header_name: &str,
    header_value: &str,
) {
    resp.headers()
        .get(header_name)
        .map(|v| assert_eq!(v, header_value))
        .unwrap_or_else(|| panic!("No header '{}' returned.", header_name))
}

async fn fetch_body_bytes(resp: &mut Response<Body>) -> Bytes {
    let mut body = resp.take_body();
    let mut bytes = BytesMut::new();
    while let Some(item) = body.next().await {
        bytes.extend_from_slice(&item.unwrap());
    }
    bytes.freeze()
}

pub fn fetch_body(resp: &mut Response<Body>) -> String {
    let bytes = executor::block_on(fetch_body_bytes(resp));
    String::from_utf8(bytes[..].to_vec()).unwrap()
}
