use typed_html::dom::DOMTree;

pub trait StringRepl {
    fn repl(&self, name: &str, subs: DOMTree<String>) -> String;
}

impl StringRepl for String {
    fn repl(&self, name: &str, subs: DOMTree<String>) -> String {
        self.replace(name, &subs.to_string())
    }
}
